﻿namespace MikeCZ.Utilities.FileLogger
{
    public class FileLoggerConfiguration
    {
        public string FileName { get; set; } = Path.Combine(AppContext.BaseDirectory, "log.txt");
    }
}
