﻿using Microsoft.Extensions.Logging;

namespace MikeCZ.Utilities.FileLogger
{
    public class FileLogger(string name, Func<FileLoggerConfiguration> getCurrentConfig) : ILogger
    {
        public IDisposable? BeginScope<TState>(TState state) where TState : notnull => default!;


        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }

            FileLoggerConfiguration config = getCurrentConfig();
            using StreamWriter sw = File.AppendText(config.FileName);
            sw.WriteLine($"{DateTime.Now:O}");
            sw.WriteLine("----------------------------------------------------------------------------------------------------");
            sw.WriteLine($"[{eventId.Id}:{logLevel}] {name}-{formatter(state, exception)}");
            sw.WriteLine("----------------------------------------------------------------------------------------------------");
            sw.WriteLine();
        }
    }
}
